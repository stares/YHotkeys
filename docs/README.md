---
description: >-
  Yunus Emre Ak ~ YEmreAk (@yedhrab) 'ın windows 10 kısayolları artıran YHotkeys
  adlı uygulaması
---

# 🌱 YHotkeys

## 🔰 Ne İşe Yarar

⭐ Tek bir fide bile, Windows'unu tazelemeye yeter.

* 👮‍♂️ Windows'un kısıtlı kısayollarına alternatif olarak yapılmıştır
* ✨ Windows üzerinde yeni [💞 Kısayollar](kisayollar/) kullanmanızı sağlar
* 🥰 Emoji kısayolları olan [🚀 YEmoji](yemoji.md) ile her yerde çalışan emoji kısayolları sunar

## 👣 Kullanım Adımları

* ⏬ Uygulamanın son sürümünü [🔗 buradan](https://github.com/yedhrab/YHotkeys/releases/latest) indirin
* 👨‍🔬 Deneysel sürümünü  [🧪 buradan](https://github.com/yedhrab/YHotkeys/raw/master/build/YHotkeys-Installer.exe) indirebilirsin
* 👀 Uygulamanın sunduğu kısayollar için [💞 Kısayollar](./#kisayollar) alanına bakabilirsin
* 🥰  YEmoji kısayolları için [🚀 YEmoji](yemoji.md) alanına bakmalısın
* 🌱 İkonuna tıklayarak bu sayfaya yönelebilirsin

{% hint style="info" %}
👀 Diğer sürümlere göz atmak için [🏷️ Release](https://github.com/yedhrab/YHotkeys/releases) alanına bakmalısın
{% endhint %}

## ✨ Güncelleme İşlemleri

* 🖱️ İmlecini 🌱 ikonunun üzerinde bekleterek, kullandığın sürümü görebilirsin
* ✨ Uygulama kendini otomatik olarak güncellemektedir
* 📋 Güncellemeler hakkında detaylara [✨ Changelog](changelog.md) alanında erişebilirsin

{% hint style="info" %}
📢 Güncellemede sorun yaşarsan [👣 Kullanım Adımları](./#kullanim-adimlari) alanından son sürümü indirebilirsin
{% endhint %}

## 🚩 System ile Başlatma

* ✨ Scriptinizin kısayolunu veya kopyasını oluşturun
* 🎌 ❖ Win R tuşlarına basıp `shell:startup` yazıp ↩ Enter 'a basın
* 🚙 Oluşturduğunuz kısayolu veya kopyayı açılan dizine kopyalayın

## 👁‍🗨 Uygulamaya Bakış

![](.gitbook/assets/usage%20%282%29.gif)

![](.gitbook/assets/tray_icon-1.png)

![](.gitbook/assets/tray_hover-1.png)

![](.gitbook/assets/tray_menu-1%20%281%29.png)

## 💖 Destek ve İletişim

​[​![Github](https://drive.google.com/uc?id=1PzkuWOoBNMg0uOMmqwHtVoYt0WCqi-O5)​](https://github.com/yedhrab) [​![LinkedIn](https://drive.google.com/uc?id=1hvdil0ZHVEzekQ4AYELdnPOqzunKpnzJ)​](https://www.linkedin.com/in/yemreak/) [​![Website](https://drive.google.com/uc?id=1wR8Ph0FBs36ZJl0Ud-HkS0LZ9b66JBqJ)​](https://yemreak.com/) [​![Mail](https://drive.google.com/uc?id=142rP0hbrnY8T9kj_84_r7WxPG1hzWEcN)​](mailto::yedhrab@gmail.com?subject=YHotkeys%20%7C%20Github)​

​[​![Patreon](https://drive.google.com/uc?id=11YmCRmySX7v7QDFS62ST2JZuE70RFjDG)](https://www.patreon.com/yemreak/)

## 🔏 Lisans

**The** [**Apache 2.0 License**](https://choosealicense.com/licenses/apache-2.0/) **©️ Yunus Emre Ak**

![YEmreAk](https://drive.google.com/uc?id=1Wd_YLVOkAhXPVqFMx_aZyFvyTy_88H-Z)

