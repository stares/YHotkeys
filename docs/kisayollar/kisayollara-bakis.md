---
description: YHotkeys kısayollarına bakış ve tavsiyeler
---

# 👀 Kısayollara Bakış

## 💖 Kısayolların Avantajları

| 🌟 Tavsiyeler | 🔗 Bağlantı |
| :--- | :--- |
| 👁‍🗨 Pencereleri **görev çubuğunda** veya **tray icon** olarak gösterip / gizlemenizi sağlar | [✴️ Pencere Yönetimi](pencere-yoenetimi.md) |
| 📌 Çok sık baktığınız pencereleri sabitleyebilirsiniz \(❖ Win ⌂ Space \) | [✴️ Pencere Yönetimi](pencere-yoenetimi.md) |
| 📋 Seçili metinleri çalıştırabilir, google üzerinde aratabilir veye çevirebilirsiniz | [👀 Kısayollara Bakış](kisayollara-bakis.md) |
| 🔳 F1 butonu ile tam ekran olmayı desteklemese de pencereyi tam ekran yapabilirsin | [✴️ Pencere Yönetimi](pencere-yoenetimi.md) |
| [🚀 YEmoji](../yemoji.md) emojilerini kısayollarla kullanmana olanak sağlar | [🚀 YEmoji](../yemoji.md) |
