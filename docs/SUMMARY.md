# Table of contents

* [🌱 YHotkeys](README.md)
* [💖 Katkıda Bulunma Rehberi](contributing.md)
* [✨ Changelog](changelog.md)
* [💞 Kısayollar](kisayollar/README.md)
  * [👀 Kısayollara Bakış](kisayollar/kisayollara-bakis.md)
  * [📋 Seçili Metin Yönetimi](kisayollar/secili-metin-yonetimi.md)
  * [✴️ Pencere Yönetimi](kisayollar/pencere-yoenetimi.md)
  * [📂 Dizin Yönetimi](kisayollar/dizin-yoenetimi.md)
* [🚀 YEmoji](yemoji.md)

## Derlenmiş Çalışmalarım

* [📖 Kişisel Kütüphanem](https://lib.yemreak.com)
* [👨‍🏫 Öğrenme Yolları](https://learn.yemreak.com)
* [🐍 Python Notlarım](https://python.yemreak.com)
* [🧠 Yapay Zeka Notlarım](https://ai.yemreak.com)
* [🎇 Windows 10 Notlarım](https://windows.yemreak.com)
* [🐧 Linux Notlarım](https://linux.yemreak.com)
* [📊 Veri Bilimi Notlarım](https://ds.yemreak.com)
* [📲 Android Notlarım](https://android.yemreak.com)
* [☕ Java Notlarım](https://java.yemreak.com)
* [🏫 Üniversite Notları](https://iuce.yemreak.com)
* [📕 Git Notları](https://book.git.yemreak.com/)
* [🔥 Firebase ve React Projem](https://github.com/yedhrab/YReact-Firebase)
* [👨‍💻 Kişisel Scriptlerim](https://github.com/YEmreAk/YScripts)

