---
description: YHotkeys ile pencere yönetim kısayolları
---

# ✴️ Pencere Yönetimi

## 🌄 Genel İşlemler

| ⌨️ Buton | 📑 Açıklama |
| :--- | :--- |
| ⎇ Alt " | 💫 Aynı tür pencereler arasında gezinme |
| ❖ Win ⌂ Space | 📌 Aktif olan pencereyi sabitleme / sabitlemeyi kaldırma |
| F1 | 🔳 Tam ekran yapma \(desteklemese bile\) |

## 👁️ Gizle / Göster

* 👁‍🗨 Pencereleri  **tray icon** olarak gösterip / gizlemenizi sağlar
* 🌃 Uygulamalar **arka planda** gizli olarak çalışmaya devam eder
* 💨 Animasyonu gizlediği için uygulamalar **çok hızlı** bir şekilde başlatılır

| ⌨️ Buton | 📑 Açıklama |
| :--- | :--- |
| ❖ Win E | 📁 File Explorer |
| ❖ Win W | 📞 WhatsApp Desktop |
| ❖ Win T | 👪 Microsoft Teams (telegram ile değişebilir 👨‍🔬) |
| ❖ Win G | 🐙 GitHub Desktop |
| ❖ Win C | 📅 Google Calendar |

## 👀 Küçült / Göster

* 👁‍🗨 Pencereleri **görev çubuğunda** gösterip / gizlemenizi sağlar
* 🍢 Uygulamaları görev çubuğunda saklar \(gizli değildir\)
* 🌠 Görev çubuğundan geldikleri için animasyon gösterir

| ⌨️ Buton | 📑 Açıklama |
| :--- | :--- |
| ❖ Win Q | 📝 One Note |

