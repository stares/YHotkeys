---
description: YHotkeys'de neler değişti
---

# ✨ Changelog

## 📋 Tüm Değişiklikler

* [🏷️ Release](https://github.com/yedhrab/YHotkeys/releases) alanından tüm değişikliklere bakabilirsiniz.

## 👨‍🔬 2.4.2.2b

⚗️ Deneysel versiyon

- 💦 `Page` butonlarının değişiklikleri kaldırıldı
- 💦 Gereksiz kısayollar temizlendi

## 🏷️ 2.4.2.1

✨ Seçili metnin üzerine uygulanan işlemler geliştirildi

- 🌍 HTTP linklerini (`http://` ön eki olmasa bile) ❖ Win ✲ Ctrl E ile browser ile açma
- 🧠 Daha verimli HTTP link algılayıcı eklendi (`www.`, `.com` metinleri ile HTTP algılanmakta)
- ➕ YEmojiler artırıldı
- 👪 Microsoft Teams kısayolu eklendi (❖ Win T)
- 🖤 Seçili yolu komut istemi üzerinden açma kısayolu eklendi ❖ Win ✲ Ctrl C

## 🏷️ 2.4.2

🔨 Seçili Metni Düzeltme kısayolları eklendi

- 🤩 Upper, Lower, Title case, Encode ve Decode kısayolları eklendi
- ➕ YEmojiler artırıldı 🦅🦋🐊🦖🕊️🦜🦢
- 🏹 Yön YEmojileri eklendi ⬆️⬇️➡️⬅️↔️➡️⬅️↘️↗️↙️↖️
- 💦 Focus yardımcı paketi kaldırıldı

## 🏷️ 2.4.1.4

👨‍🔧 Hatalar düzeltildi, verimlilik artırıldı

* 🐞 Birden fazla aynı pencerenin olması durumunda hatalar mevcut
* 👨‍🔧 Bir önceki pencereye odaklan sorunu giderildi
* 👨‍🔧 Seçili metin kısayollarındaki çalışmama sorunu giderildi
* ➕ YEmojiler artırıldı 🤐😬🤩🛒🧺🕯️🙊🙈🙉✋🛑🚏🏊‍♂️🏄‍♂️🚴‍♂️🚵‍♂️
* 🙋‍♀️ Kadın YEmojiler eklendi

## 🏷️ 2.4.1.3

💫 Genel düzeltmeler ve iyileştirmeler

* ➕ YEmojiler artırıldı 🤘🆔💡😵
* ➕ Sayılar hakkında yemojiler eklendi 1️⃣2️⃣3️⃣
* 👪 YEmojiler gruplandırıldı ❤️🤍🤎🖤💚💜💛💙💜
* 👨‍🔧 Gecikmeli pencere açılma sorunu giderildi
* 👨‍🔧 ❖ Win E ile artık `Documents` değil `Quick Access` alanı açılmakta
* 🚀 YEmojiler kategorizelendi ve yenilendi

## 🏷️ 2.4.1.2

✨ Genel geliştirmeler

* 👨‍🔧 Pencere geçişlerindeki sorunlar giderildi
* ➕ Yemojiler artırıldı
* 👨‍🔬 Beta yönetimi eklendi

## 🏷️ 2.4.1.1

👨‍🔧 Genel hata düzeltmesi, çoklu pencere ve odak yönetimi

* 💨 Pencere geçişleri hızlandırıldı
* 🚀 Birden fazla pencereyi kısayol ile gösterip gizleme eklendi
* 🔉 İmleci görev çubuğuna getirip, fare tekerleği ile ses açıp kısma
* 👁️ Metin işlemleri; seçili metin varsa onu kopyalar, yoksa en son seçileni kullanır
* 👨‍🔧 Kısayollar ile pencere açmadaki ufak sorunlar düzeltildi
* 👨‍🔧 Pencerelere odaklanma sorunu giderildi
* 👨‍💼 ⇧ Shift butonu yerine ✲ Ctrl butonu ile çeviri, googleda arama vs işlemleri yapılmakta
* 🚫 ❖ Win F1 kısayolu ile scripti susturma / çalıştırma

## 🏷️ 2.4.1.0

🚀 Yükleyici yapısı ve tasarımda köklü değişiklik

* 📋 Alt menü oluşturuldu
* 💖 Tüm ikonlar yeniden yapılandırıldı
* 👷‍♂️ Yükleyici oluşturuldu
* 📂 Seçili metin ❖ Win ⇧ Shift E kısayolu ile dosya gezgininde açılır
* ⌨️ Klavye butonları temsil eden yemojiler eklendi
* ✨ Yemojiler artırıldı
* 👨‍💻 Kodda verimlilik

![](.gitbook/assets/tray_menu-1%20%282%29.png)

> 😥 Kısayollar henüz desteklenmemektedir.

## 🏷️ 2.4.0.2

🌍 İkona tıklandığında doküman sayfasına yönlendirme

* 🚀 Yeni doküman sayfası oluşturuldu
* 🔗 Artık ikona tıklandığında yeni web sayfasına yönlendirecek
* 🧹 İkona tıklandığında son yapılan eylemi tekrarlama kaldırıldı
* 👨‍💻 Kodlarda iyileştirme yapıldı

## 🏷️ 2.4.0.1

👨‍🔧 Yeni Kısayol ve Hata Düzeltmeleri

* 💫 ⎇ Alt " kısayolı ile aynı tür pencereler arasında gezinme
* 👨‍🔧 Sabitlemede pencere başlığından kaynaklanan sorun giderildi
* 🧹 Sabitlenen pencerelerde pencere başlığı artık değiştirilmemekte
* ➕ YEmojiler artırıldı

## 🏷️ 2.4.0

🚀 Güncelleme aracı bağımsızlaştırıldı

* 🚀 Güncelleme işlemi artık, çalışan scripti güncelliyor ve yenisini açıyor
* ✨ Güncelleme aracı scriptten bağımsız hale getirildi
* 👮‍♂️ Güncelle menüsü ile artık istendiği zamanda güncelleme kontrol edilebilmekte
* ➕ Yemojiler artırıldı

> 🚅 Program açıldığında otomatik güncelleme kontrolü devam etmektedir

## 🏷️ 2.3.3.2

📝 Not defterine hızlıca not alma kısayolu

* 📝 Seçili alanı ❖ Win ⇧ Shift N kısayolu ile `notepad` 'a aktarılıp sabitlenir
* 👨‍🔧 Güncellemedeki sorunlar giderildi
* ✨ Emojiler artırıldı

## 🏷️ 2.3.3.1

👨‍🔧 Genel Sorun Düzeltme

* 👨‍🔧 Güncellemedeki sorunlar giderildi
* ✨ Emojiler artırıldı

## 🏷️ 2.3.3

📌 Sabitlenen pencereler daha belirgin

* ✨ Güncellemeden sonra yeni script çalıştırılmakta
* 👮‍♂️ Daha güvenli güncelleme sistemi kuruldu
* 🌃 Sabitlenen ekranlarda şeffaflık efekti oluşturulmakta
* 🌄 Uygulama kapatıldığında sabitlenen tüm pencereler eski haline alınmakta
* ✨ Pencerelerin görünme eylemlerinde ufak iyileştirmeler yapıldı
* 🚀 YEmoji geliştirildi, kısayolları arttı

## 🏷️ 2.3.2

👨‍🔧 Hata düzeltmeleri ve Güzelleştirmeler

* 💫 Güncelleme alanı detaylandırıldı
* 📑 Sabitlenen pencerelerin başlıklarına 📌 konulmakta
* 🔳 F1 butonu ile pencereleri tam ekran yapma, yapılı ise normale alma özelliği eklendi
* 👨‍🔧 Sanal masaüstü değiştirme sorunu düzeltildi, artık gizlenen pencereler aktif masaüstüne getirilecek
* 👨‍🔧 Eski pencereye odaklanmada meydana gelen sorun giderildi
* 👨‍💻 Fonksiyon isimleri sadeleştirildi, kod yapısı anlamlaştırıldı

## 🏷️ 2.3.0

🚀 Güncelleme yapısı eklendi

* ✨ Kendi kendisini güncelleme yapısı eklendi
* 💖 Emojiler artırıldı
* 👨‍💻 Kodlamada iyileştirme yapıldı
* 📂 Dosyalar yeniden yapılandırıldı

